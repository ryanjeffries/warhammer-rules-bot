package main

import (
	"strconv"
	"time"
	"fmt"
	"strings"
	"gitlab.com/ryanjeffries/telegram-bot/telegrambot"
)

const DELAY = 1

func subscriber(token string) {
	var delay = DELAY * time.Second
	var offset = telegrambot.GetCurrentOffset(token)
	for {
		var updates = telegrambot.GetUpdates(token, telegrambot.GetUpdateMethodRequest{Offset: offset})
		offset = processUpdates(updates, offset, token)
		time.Sleep(delay)
	}
}

func processUpdates(updates []telegrambot.Update, offset int, token string) int {
	var newOffset = offset
	for _, update := range updates {
		if update.Message != nil {
			newOffset = update.UpdateID + 1
			if update.Message != nil {
				updateHandler(update, token)
			}
		}
	}
	return newOffset
}

func updateHandler(update telegrambot.Update, token string) {
	var inputs = strings.Fields(update.Message.Text)
	for _, input := range inputs {
		if rule := strings.ToLower(strings.Replace(input, "@WarhammerBot", "", -1)); rules.Key(rule) != nil {
			var a = fmt.Sprintf("%+v", rules.Key(rule))
			var b = strings.Replace(a, "\n", "\n\n", -1)
			telegrambot.SendMethodRequest(token, telegrambot.SendMessageMethodRequest{Text: "*" + rule + "*" + b, ParseMode: "markdown", ChatId: strconv.Itoa(update.Message.Chat.ID)})
		}
	}
}