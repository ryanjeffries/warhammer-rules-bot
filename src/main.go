package main

import (
	"gitlab.com/ryanjeffries/warhammer-bot-secrets/warhammerBotSecrets"
	"gitlab.com/ryanjeffries/telegram-bot/telegrambot"
)

var token = warhammerBotSecrets.GetSecrets().Token
var rules = loadRules()

func main() {
	go subscriber(token)
	telegrambot.HandleRequests(&telegrambot.Address{Port: "8443", Ip: "", Token: token})
}