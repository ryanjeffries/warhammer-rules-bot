package main

import (
	"github.com/kylelemons/go-gypsy/yaml"
	"path/filepath"
	"strings"
	"fmt"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func nodeToMap(node yaml.Node) (yaml.Map) {
	m, ok := node.(yaml.Map)
	if !ok {
		panic(fmt.Sprintf("%v is not of type map", node))
	}
	return m
}

func loadRules() (yaml.Map) {
	var path, e = filepath.Abs("."); check(e)
	var fullPath = path + "/dictionary.yml"
	fmt.Println("fullpath: " + fullPath)
	file, e := yaml.ReadFile(fullPath); check(e)
	return nodeToMap(file.Root)
}

func convertYamlKeyToLowerCase() {
	var rules = loadRules()

	for k := range rules {
		fmt.Println("" + strings.ToLower(k) + " - Special Rule")
	}
}